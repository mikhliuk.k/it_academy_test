test_matrix = [[1, 2, 3, 4],
               [5, 6, 7, 8],
               [9, 1, 2, 3],
               [4, 5, 9, 7]]


def det(matrix):
    if len(matrix) == 2:
        return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0])

    answer = 0

    for line_index, line in enumerate(matrix):
        answer += (-1) ** line_index * matrix[0][line_index] * det(delete_row_and_column(matrix, line_index))

    return answer


def delete_row_and_column(matrix, removed_column_index):
    temp_matrix = list(map(lambda line: line.copy(), matrix))
    del temp_matrix[0]

    for column in temp_matrix:
        del column[removed_column_index]

    return temp_matrix


print(f"Determinant: {det(test_matrix)}")
