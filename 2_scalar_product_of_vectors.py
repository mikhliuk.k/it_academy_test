import argparse

parser = argparse.ArgumentParser(description='Function that implements the scalar product of two vectors of arbitrary '
                                             'length.')
parser.add_argument('--vector1', type=int, nargs='+', required=True)
parser.add_argument('--vector2', type=int, nargs='+', required=True)
args = parser.parse_args()

if len(args.vector1) != len(args.vector2):
    raise Exception('Different number of elements in the \'vector1\' and \'vector2\' params.')

vector1 = args.vector1
vector2 = args.vector2

scalar_product = 0
for vector_index, vector_value in enumerate(vector1):
    scalar_product += vector_value * vector2[vector_index]

print(f"Scalar product: {scalar_product}")
