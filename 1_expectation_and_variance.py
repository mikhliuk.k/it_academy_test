import argparse

parser = argparse.ArgumentParser(description='Write a function that receives at the input the numerical values of a '
                                             'certain random variable and the probability of their occurrence, '
                                             'and the return value is its expectation and variance.')
parser.add_argument('--values', type=int, nargs='+', required=True)
parser.add_argument('--probabilities', type=float, nargs='+', required=True)
args = parser.parse_args()

if len(args.values) != len(args.probabilities):
    raise Exception('Different number of elements in the \'values\' and \'probabilities\' params.')

for probability in args.probabilities:
    if probability > 1:
        raise Exception(f"Incorrect probability {probability}. Must be no more than 1")

values = args.values
probabilities = args.probabilities
expectation = 0
expectation_2 = 0

for value_index, value in enumerate(values):
    expectation += value * probabilities[value_index]
    expectation_2 += value ** 2 * probabilities[value_index]

variance = expectation_2 - expectation ** 2

print(f"Expectation: {expectation}, Variance: {variance}")
