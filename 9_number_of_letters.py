import argparse

parser = argparse.ArgumentParser(description='The latest edition of the British Encyclopedia has N pages, each page '
                                             'has its number. Crazy typesetter has calculated that it took M letters '
                                             'to dial all the page numbers. Your task for this number of letters M, '
                                             '1≤M≤109, is to determine the number of pages N. The number M is such '
                                             'that it corresponds to a certain number N. The program receives the '
                                             'number M, the program must output the number N..')

parser.add_argument('M', type=int)
args = parser.parse_args()

liters_count = args.M
page_count = 0

while True:
    if liters_count <= 0:
        break

    page_count += 1
    liters_count -= len(str(page_count))


print(f"Page numbers: {page_count}")
